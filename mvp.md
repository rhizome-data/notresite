---
layout: default
title: Le produit d'impact
---

### Piloter par la valeur ajoutée

Le MVP ou produit à valeur ajoutée est une réalisation permettant d'**engager le dialogue** entre les personnes en charge du développement d'une idée ou d'un projet et celles ou ceux à qui il est censé s'adresser. Cette approche **permet de valider les hypothèses** auprès de communautés identifiables (des agents publics, des citoyen.nes, des personnes qui fréquentent un site Internet ou une salle de lecture, etc.) pour et de reçevoir leur idées sur l'adéquation de cette proposition initiale par rapport à leur besoin.

![la parallélisation des attentes](images/MVP_PAS_MVP.png)

Elle permet d'engager un minimum de ressources et de coller au besoin de la cible identifiée en amont. Elle garantie également l'**implication** des parties prenantes et la transparence de la feuille de route décidée collectivement.

Pour déclencher cette phase il est nécessaire de partir d'une hypothèse de besoin mais dont la définition reste assez vague. La suspicion d'avoir découvert un irritant est suffisamment forte pour déclencher la construction d'un MVP (minimum viable product, ou produit minimum viable).

Un binôme est constitué, composé d'un·e Product-Manager (PM ou responsable produit) et d'un·e membre de notre équipe.
Le ou les problèmes rencontrés sont caractérisés pour que l'ensemble des parties prenantes les identifient. Les solutions de contournement éventuelles sont analysées afin de s'assurer que la ou les solutions proposées apportent bien de la valeur.
Après une séquence d'interviews des parties prenantes, le ou la PM commence à formuler des hypothèses pour adopter et qualifier l'irritant.
Après une séquence d'interviews des parties prenantes, le ou la PM commence à formuler des hypothèses pour adopter et qualifier l'irritant.

### Hypothèses

- Partir sur quelques échantillons exemplaires d'usagers irrité·e·s partageant des pratiques, et des affinités culturelles.
- Imaginer quelques problèmes éligibles pour qualifier l'irritant.
- Proposer des hypothèses d'alternatives utilisées par les usagers pour contourner l'irritant.

Une séquence d'interviews, et de re-modelage permettent de consolider ces hypothèses, et d'obtenir une compréhension suffisante pour que le ou la PM puisse se mettre en sympathie avec les usagers. En outre, les usagers indiquent des profils pour étoffer la future communauté pilote.

#### Exemple d'adaptation

- Lorsque le métier est difficile à dénouer, on pratique des ateliers de découvertes du domaine, selon le format d'Event Storming par exemple.
- Si l'on souhaite impliquer l'ensemble des act·eur·rice·s on peut recourir à un Forum-Ouvert suivi d’ateliers de co-construction citoyenne.

### Phase d'expression

Le ou la PM imaginent avec le ou la consultant.e quelques fonctionnalités éligibles pour répondre aux problèmes racines. Une solution est exprimée sous la forme d'un démonstrateur et d'un ensemble d'artefacts de présentation.

- Hypothèses formulées :
  - Une proposition unique de valeur est énoncée.
  - Des outils de diffusion sont pressentis pour que les usagers rencontrent le produit - Channels.
  - On vérifie également l'engagement des usagers en vérifiant par exemple, le prix, les recommandations aux proches ou le temps qu'ils ou elles pourraient consacrer pour pouvoir utiliser le produit.

Une nouvelle séquence d'interviews démarre avec les usagers pour modeler cette première solution embryonnaire et affermir la proposition unique de valeur.

### Phase de réalisation du MVP

Le ou la PM, les parties prenantes et le ou la consultant.e réalisent et déploient le MVP.

- Hypothèses formulées :
  - Des métriques pour invalider ou confirmer l'expérimentation.
  - Formulation des caractéristiques de singularité - unfair advantage

Le produit est mis à disposition des usagers. Ils et elles prennent possession du produit, le ou la PO planifie les tests d'utilisabilité, et facilite l'expression des retours des usagers - feedback

Le MVP est consolidé en tenant compte des retours et des découvertes.

Un canvas peut être utilisé comme support afin de définir en atelier les réponses aux questions suivantes :

1. **Identifier les parties prenantes** : qui sont les premiers utilisateurs ou les premierès utilisatrices de la solution mise en place -> Pour-qui
2. **Identifier leurs problèmes** : quelles sont les éventuelles solutions (de contournement) qu’ils ou elles utilisent -> Quoi
3. **Identifier les solutions** disponibles -> Comment
4. **Identifier les critères d’évaluation** de la valeur apportée par la solution envisagée -> A quoi ça sert
5. **Identifier les critères de légitimité** du(des) porteur de projets -> Pourquoi nous
6. **Identifier les canaux de communication** mobilisables pour faire parler de son projet -> Qui le sait
7. **Identifier le concept à haute valeur ajoutée** et la proposition de valeur associée au projet -> C'est quoi ?
8. **Identifier les coûts** -> Combien ça coûte
9. **Identifier les revenus** ou les gains - Qu'est ce que cela nous apporte ?



## Déroulement proposé de la prestation

### Définition du rythme soutenable

Nos interventions sont basées sur les **rythmes soutenables** de l'implication des collaborateurs ou collaboratrices. " Si l'échéance de mon projet est à un an ou même six mois, il est possible que je perde ma motivation ou mon implication en cours de route." 

### Jalons de restitution

Pour garantir l'implication du donneur d'ordre et lui permettre d'**exprimer le plus souvent possible son consentement** ou **la réorientation de ses priorités**, nous vous proposons des restitutions quinzomadaires. Chaque jalon de travail est de durée égal et démarre, sauf cas de force majeure, un lundi pour se clôturer un vendredi.



### Suivi quotidien

En début de prestation une réunion quotidienne de **très courte durée** permet à chacune ou chacun des membres du projet de communiquer sur ses activités et les éventuels blocages auxquels il ou elle fait face.
Cette réunion est informative et ne doit pas constituer une contrainte.

### Rétrospective

Une restitution du travail incrémental en cours est proposée à l'ensemble des parties prenantes en général le vendredi et **une rétrospective de la séquence de travail** terminée est proposée aux intervenant.es afin de vérifier s'ils sont à l'aise avec le mode d'organisation mis en oeuvre et si des axes d'améliorations peuvent être collectivement identifiés.
