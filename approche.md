---
layout: page
title: Modalités d'accompagnement
---

## Valeurs

Imprégnés des valeurs du service public, nous souhaitons oeuvrer en faveur du **développement du bien commun**, notamment au travers de l'utilisation de **logiciels libres** et par la mise à disposition d'une documentation sous **licence libre**.

Nous ne souhaitons pas nous positionner comme experts mais plutôt comme **facilitateur.trice** ou accompagnateur.trice en privilégiant les valeurs d'écoute, de respect et de transparence.

## Méthodologie

A partir de la vision commune élaborée avec les parties prenantes, nous établissons avec vous un canvas en nous inspirant de la culture lean afin d'identifier les éléments qui comportent le plus de valeurs d'usages, qu'il soient techniques, réglementaires ou fonctionnels. Cette première identification de la **valeur maximale d'usage** permet de construire un objectif commun et d'identifier la manière dont chacun pourra contribuer à sa réalisation.

![essaim](images/eventStorming.png)

En parallèle, des ateliers d'expression du besoin permettent de construire un référentiel commun d'attentes et de commencer à travailler sur leur **priorisation** et l'identification des moyens nécessaires pour leur mise en oeuvre.

Notre inspiration vient du monde de l'agilité. Cette approche part des besoins des utilisateur.trices et privilégie une **construction incrémentale** associant l'ensemble des parties prenantes.

La première prise de contact doit en effet permettre de prendre connaissance du contexte particulier du projet et des personnes avec lesquelles nous allons interagir. En fonction de ces éléments, notre méthodologie vise à identifier **le périmètre initial** en mesure d'apporter de la valeur ajoutée. Ce jalon initial vise également à installer une relation de confiance et de garantir que l'ensemble des parties prenante est à l'aise avec la direction choisie pour initier le projet.

Cette première phase se clôture par une rétrospective avec l'ensemble des parties prenantes impliquées dont l'objectif est de prendre du recul sur ce qui a été mis en oeuvre et d'identifier les axes d'amélioration.

![struture de convergence](images/convergenceStructure.jpg)

Tout au long du projet, les tâches mises en oeuvre sont décidées collectivement et le plan de réalisation est affiché pour être porté à la connaissance de toutes et tous.

L'objectif de nos missions de formation et/ou d'accompagnement est de transmettre des connaissances ou des compétences afin de rendre autonome nos client.es. Cette transmision s'effectue tout au long du projet par une interaction, si possible quotidienne avec le référent ou la référente désigné.e par le client ou la cliente. Cette modalité d'échange vise à garantir l'**implication** des parties prenantes et la **transparence** de nos activités.
Cette transmission s'effectue également en fin de mission par la transmission de livrables sous licence ouverte, librement modifiables et reproductibles.
