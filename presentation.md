---
layout: page
title: Qui sommes-nous ?
---

Nous avons créé Rhizome data à deux. 

Pour paraphraser Deleuze, comme chacun de nous était plusieurs, ça faisait déjà beaucoup de monde.

Nous avons adopté la démarche du système-radicelle, ou racine fasciculée pour le pont qu'elle opère entre la production documentaire devenu archives et les flux et espaces au sein desquels elle se diffuse.

## Des jardinier.e.s numériques

Nous ne souhaitons pas nous positionner comme experts mais plutôt comme facilitateur.trices ou accompagnateur.trices en privilégiant les valeurs d'écoute, de respect et de transparence.

Imprégnés des valeurs du service public, nous souhaitons oeuvrer en faveur du **développement du bien commun**, notamment au travers de l'utilisation de **logiciels libres** et par la mise à disposition d'une documentation sous **licence libre**.

## Nos valeurs

Venant du monde des collectivités territoriales, nous souhaitons, aujourd'hui, mettre en commun nos connaissances et **nos valeurs** afin d'accompagner au mieux les maitrises d'ouvrage publiques et privées dans leurs projets et former leurs équipes.

Nos champs de compétences tournent, notamment, autour du document, de la donnée et des métadonnées. Nous pouvons donc vous accompagner dans de nombreux domaines que cela soit la mise en place d'un projet d'archivage électronique, d'un portail documentaire,  de cartographie des flux ou d'un portail open data et ou les quatre en même temps !

Depuis de nombreuses années, nous travaillons, collaborons, échangeons et argumentons autour de nos visions de la donnée, de son évolution et de la **nécessaire complémentarité des points de vue**. Le monde de la donnée permet à deux mondes autrefois dissociés de se connecter et notre collaboration est un exemple de fusion des domaines de compétences des archivistes et des informaticiens.  

**Chacun apporte sa spécificité tout en complétant l'autre.**

De ce fait, nous sommes capables d'aborder et de maîtriser un grand nombre de sujets liés au domaine des archives et donc de la donnée.

Chacun dans nos domaines, nous avons participé à de nombreux projets autour de la **gestion du cycle de vie ou de la qualité des données** comme la rétroconversion des instruments de recherche d'archives, la numérisation d'archives, l'élaboration de sites internet et d'outils de diffusion des informations patrimoniales et administratives (portail open data). Nous avons aussi participé à la conception et à la mise en oeuvre d'un système d'archivage électronique orienté "métadonnées".

Notre expérience en terme de gestion de projet nous a naturellement orienté vers des méthodes alternatives aujourd'hui reconnues pour leur capacité à accompagner les stratégies de transformation numérique.