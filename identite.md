---
layout: default
title: Identité
---

## Dénomination

Adresse:
Rhizome-data

5 rue Marc Nouaux

33150 Cenon

Tél : 0954195575

[email](mailto:contact@rhizome-data.fr)

Siret : FR19 847845930 00019/ APE : 9499Z / NAF : 6311 Z

Categorie juridique : association déclarée

## Bureau

Président : Hervé Girardot

Trésorière : Delphine Jamet

## Réseaux sociaux

[twitter](https://www.twitter.com/rhizomedata)

[gitlab](https://gitlab.com/rhizome-data)

## Banque

fr68 2004101001219312 8002 236

bic psstfrppbor

20041 01001 2193128D022 36
