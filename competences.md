---
layout: page
title: Nos atouts
---

- Une culture forte des **missions du service public**,
- Une très bonne connaissance du cadre réglementaire de la commande publique,
- Une pratique de la **gestion de projet inspiré des méthodologies de la communauté agile et lean** (scrum, kanban, lean canvas),
- Une connaissance fine du **métier d’archiviste** et des contextes réglementaires et normatifs associés,
- Une connaissance des **différentes solutions techniques** existantes et de leurs couvertures fonctionnelles,

Et bien entendu, la connaissance du métier du conseil, en particulier pour ce qui est des capacités des consultants à écouter, analyser, formaliser, synthétiser et préconiser de manière pragmatique et efficace les meilleurs scenarii possibles.

## Compréhension des besoins

Notre expérience du secteur public et plus particulièrement des processus de gestion du cycle de vie de l'information nous permettent d'appréhender le contexte particulier de chaque client et de l'accompagner dans l'expression de ses besoins.

![animation de la ruche](images/Implication-client_Projet-web.jpg)

Parce que nous connaissons les contraintes et le mode d'organisation des collectivités publiques, nous proposons systématiquement une approche incrémentale de réponse aux besoins. Nous privilégions **l'opérationnalité à la plannification** et nous cherchons systématiquement à vérifier les hypothèses formulées au départ par les donneurs d'ordre en rencontrant les personnes les plus impactées par le **projet de transformation**.

Pour valider avec les parties prenantes notre compréhension de vos besoins, **nous affichons les éléments de notre compréhension au fur et à mesure dans un espace ouvert** (physique et/ou numérique) afin de favoriser l'échange et les discussions. Nous recherchons à identifier avec vous les leviers qu'il est possible d'activer rapidement.

Au travers de cette approche, nous vous accompagnons dans l'identification des [éléments de valeur ajoutée](mvp.html) (à quoi ça sert, à qui ça sert) et l'amélioration continue des livrables attendus.

Nous ne préconisons pas de rédaction, au sens conventionnel du terme, de cahier des charges. Nous proposons plutôt d'utiliser notre temps et le votre pour vous **accompagner à la mise en oeuvre agile de votre projet**. Le cahier des charges, au lieu d'être un catalogue de fonctionnalités ou d'actions devient un outil de choix de prestataires en fonction de ses compétences techniques, bien entendu, mais surtout de son adaptabilité à votre projet. L'argent économisé sur une rédaction longue et par essence incomplète est reporté sur l'accompagnement à la mise en oeuvre du projet.
