---
layout: page
title: Nos outils
---

## Outils d'animation

### Atelier d'expression de besoin

Ces ateliers permettent de stimuler la créativité et l'expression tout en identifiant les **contraintes de temps et de budget** propre au contexte. Le résultat consiste dans un référentiel des besoins. A l'intérieur de celui-ci, notre valeur ajoutée consiste à **faciliter l'identification de la valeur ajoutée** apportée par la mise en oeuvre de ces besoins. En aidant à leur priorisation et en interviewvant les personnes impliquées, il est possible de formaliser un calendrier de réalisation soutenable et valorisant.

### Kanban

Le tableau ou Kanban du projet est un dispositif de communication et de **priorisation**.

Il contient en général trois colonnes au sein desquelles sont disposées des les tâches à faire, celles en cours de réalisation et celles terminées.

Ce tableau est une **invitation à la discution** entre la liste des choses à faire et celles qu'il est effectivement possible à faire.

C'est également un **engagement de transparence** car ce tableau est disponible pour tout le monde à chaque instant et l'avancement des réalisations discuté le plus régulièrement possible.

Ce tableau est utlisé au départ du projet comme support pour l'identification de la liste des choses à faire ou **référentiel de besoin** (backlog). Il est utilisé tout au long du projet comme un **support de discussion** lors des réunions de planification de réalisation (sprint) et à la fin du projet ou à chaque étape de réalisation pour animer la rétrospective.

### Rétrospective

La rétropective est un dispositif visant à l'amélioration continue. Il permet à chacun de verbaliser son ressenti des réalisations et interactions ayant eu lieu lors de la phase de réalisation. Alternant les phases de **prise de recul objectif** et l'**expression des ressentis subjectifs individuels** il permet d'éviter les attaques ad nominem et la cristalisation des ressentiements. Cet atelier vise principalement à développement la **bientraitance** au sein du groupe et de permettre son amélioration.

## Outils de suivi et de gestion

![illustration d'un processus](images/frammindmap.png)Mindmapping

Framindmap permet de créer et partager des cartes mentales (aussi appelées « cartes heuristiques »). Pourquoi créer une carte mentale ?

- Faire un brainstorming
- Ordonner ses idées
- Réaliser des classifications
- Identifier les éléments importants

![prendre des notes collaboratives](./images/Framapad.png) prise de notes (suite framasoft)

![se voir à distance](./images/appear-in.png)Visio-conférence (appear.in)

![partager l'avancée du projet](./images/GitLab-logo.png)Le Kanban (GitLab Project) :

le Kanban (terme japonais signifiant “enseigne, panneau”) facilite la collaboration entre les membres de l'équipe agile. C'est aussi une méthode et un outil efficace pour surveiller l'amélioration continue du produit ou du service à développer.

![être joignable sans être dérangé](./images/Slack-icon.png)Messagerie instantanée - Tchat

Dans Slack, le travail d’équipe se déroule dans les chaînes, un endroit unique pour les messages notamment ; ce qui permet de collaborer facilement et de gagner du temps. Les membres des équipes peuvent rejoindre et quitter les chaînes comme ils le souhaitent. Finies les ribambelles d’e-mails interminables.

![décider collectivitement](./images/loomio-logo.png)outil d'aide à la prise de décision

Loomio est une application qui aide à prendre des décisions ensemble. Loomio vous fait gagner du temps, vous aide à obtenir des résultats clairs et vous garde tout à portée de main

![écriture collaborative de la documentation](./images/readthedocs.png)documentation (readthedoc),

![suivi des modifications des réalisations](./images/GitLab-logo.png)Dépot de sources et versionning (gitlab)
