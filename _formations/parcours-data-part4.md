
---
layout: formation
id: dataliteracy-part4
category: parcours-data
title: L'ingénierie de la connaissance
length: 2 jours (14 heures)
object: > 
    Ce module est destiné aux experts de la donnée qui souhaitent développer leurs compétences en ingénierie de la connaissance au travers des modalités les plus avancées de modélisation et d'interconnexion de vaste gisement de données. 
audience: >
    Experts de la données ou personnes ayant suivies les 3 premiers modules du parcours.
prerequisites: > 
    maîtrise des outils de production de données, solide culture de la donnée
topics:
    - exploiter la La structure du Web
    - maîtriser le cadre de description des ressource
    - pratique de la modélisation de données
    - techniques d'exploitation des données
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - parcours-data
---

### Programme

1. La structure du Web
2. les cadres de description
3. La modélisation
   1. graphe
   2. ontologie
   3. schémas
4. Exploitation des données
   1. requêtes
   2. programmation
   3. machine learning
   4. smart