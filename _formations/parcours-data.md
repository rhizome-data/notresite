---
layout: formation
id: dataliteracy
category: parcours-data
title: Parcours formation culture de la donnée
length: 7 jours (49 heures)
object: > 
    Ce parcours a été conçu de manière modulaire afin de permettre à chacun en fonction de ses besoins et de ses missions de choisir les modules adéquats.
    
    Il s'adresse tout à la fois au délégué.e.s à la protection des données, aux chargés de mission et de gestion en prise avec les enejux ou les outils numériques, les décideurs et les élus en 
    charge de définir des stratégies et de piloter des visions partagées, aux experts de la donnée souhaitant parfaire leur culture générale ou leurs compétences juridiques ou techniques, à tous les membres des administrations ou établissements publics désireux de développer leur culture générale et professionnelle.
audience: >
     Ce parcours est conçu à destination des personnels de la fonction publique et des élus afin de les sensibiliser et de leur permettre de monter en compétences sur les enjeux associés à la gestion des données.
prerequisites: > 
    aucun si ce n'est la curiosité :)
topics:
   - Les enjeux de la gestion du cycle de vie de la donnée
   - Culture générale de la donnée
   - La production de la donnée
   - La (ré-)utilisation de la donnée
   - L'ingénierie de la connaissance

adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.  
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - data
---

Ce parcours est composé de plusieurs module de 1 à 3 jours. Chaque module peut être suivi indépendamment ou envisagé comme un parcours d'acquisition de compétences.

Les liens ci-dessous détaillent le contenu de chaque module : 


* [Les enjeux de la gestion du cycle de vie de la donnée](dataliteracy-intro)
* [Culture générale de la donnée](parcours-data-part1)
* [La production de la donnée](parcours-data-part2)
* [La (ré-)utilisation de la donnée](parcours-data-part3)
* [L'ingénierie de la connaissance](parcours-data-part4)
