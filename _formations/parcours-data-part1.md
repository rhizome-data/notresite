---
layout: formation
id: dataliteracy-part1
category: parcours-data
title: Culture générale de la donnée
length: 2 jours (14 heures)
object: > 
    Ce module donne les clés d'une culture de la bidouillabilité des données afin que chacun.e s'autorise à faire soi-même en s'inspirant des bonnes pratiques en la matière.
audience: >
    agents opérationnels, encadrants opérationnels, chargés de mission.
prerequisites: > 
    aucun
topics:
    - La fabrique de la donnée
    - Les formats de structuration
    - les services de diffusion
    - le cycle de vie de la donnée
    - la mise en relation des données
adaptation: >
    Cette formation peut être adaptée afin de répondre au mieux à vos besoins.
conditions: > 
    Des sessions en inter-entreprises sont organisées régulièrement dans nos locaux à Paris et à Bordeaux.  Il est possible d'organiser, à votre demande, une ou plusieurs sessions en intra-entreprise (sur site) n'importe où en France ou en Europe. Chaque participant à la formation dispose d'un exemplaire personnel du support de cours et du manuel d'exercices.
tags:
    - web
    - parcours-data
---

### Programme

1. La fabrique de la donnée
   1. formulaires
   2. relevés
   3. rapports
   4. listes
2. Les formats de structuration
   1. le tableur
   2. la base de données
   3. les données liées
3. les services de diffusion
   1. mise à disposition de jeux de données
   2. interfaces programmatiques
   3. la contribution au bien commun
4. le cycle de vie de la donnée
   1. production
   2. partage
   3. validation
   4. diffusion
   5. archivage
5. la mise en relation des données
   1. Le Web
   2. les standards d'interopérabilité
   3. les protocoles d'échanges
