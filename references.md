---
layout: page
title: Nos références
---

### Assistance à maitrise d'ouvrage

**Département de la Charente**    ![photo](images/logo_cg16.jpg)

Mission d'assistance à maîtrise d'ouvrage pour l'accompagnement à la mise en oeuvre de l'archivage électronique

### Formations

**Association des archivistes français**    ![photo](images/logo-archivistes.gif)

* [Web de données et archives](https://rhizome-data.gitlab.io/formations/presentations/aaf/webData/formationAAF.html#1)
* [Seda : principes et mise en oeuvre](https://rhizome-data.gitlab.io/formations/presentations/ae/formation-seda-AAF.html#1)
  
**Olkoa**   ![photo](images/Olkoa.png)

* Seda : enjeux et perspectives de mise en oeuvre